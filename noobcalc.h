/*HEADER FILE FOR NOOBCALC*/

// CURSOR DEFINITIONS
#define CFW   "\033[1C"
#define CBW   "\033[1D"

/* COLOR DEFINITIONS */
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
/*BOLD def*/
//#define BOLD1  "\27[1m"
//#define BOLD1  "\27[1m"
char ESC=27;

#define CLS "\033[2J"

typedef FILE* file; //File pointer define for easier coding
file fp;

/* BASIC MATH FUNCTIONS */

float sq (float x) //second power
{
	float sqx = 0;
	sqx = x*x;
	return sqx;
}

float sum(float x, float y) //addition
{
	float sumxy = 0;
	sumxy = x+y;
	return sumxy;
}

float dif(float x, float y) //substraction
{
	float difxy = 0;
	difxy = x-y;
	return difxy;
}

float prod(float x, float y) //multiplication
{
	float prodxy = 0;
	prodxy = x*y;
	return prodxy;
}

float quot(float x, float y) //division
{
	float quotxy = 0;
	if (y == 0){ // check if second number is 0, because you can't divide by zero;
	printf("You can't divide by zero!\n");
	return 0;
	}
	else{
	quotxy = x/y;
	return quotxy;
	}
}

