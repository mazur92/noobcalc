#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include "./noobcalc.h"

float x, y, w = 0; // Main variables for values
float wcp = 0;
int mem = 0; // memory flag variable
char option = 0; // chosen option variable

int errorcheck (int ec) 

/*function that checks if the input was proper (variable initialized can be used to bypass some nested functions)*/

{
	while (ec == 0){
		switch (option){
		default: //wrong option
		ec = 2;
		//return (ec);
		//break;
		case 63: //mem query
		ec = 0; 
		return (ec);
		break;

		case 97: //addition
		ec = 0; 
		return (ec);
		break;

		case 115: //substraction
		ec = 0; 
		return (ec);
		break;

		case 100: //multiplication
		ec = 0; 
		return (ec);
		break;

		case 109: //division
		ec = 0; 
		return (ec);
		break;

		case 120: //memory
	/*if (mem == 1){ // is memory flag already up?
		mem = 0;
		fprintf(stdout, "Memory cell was cleared.\n");
		ec = 1;
		return (ec);
		break;
		}
*/

	//if (mem == 0){ // memory flag is down so we can store there our number
		/*if(w == 0){ // 0 is pointless to mem
		fprintf(stderr, "Memory cell is equal to 0.\n");
		ec = 2;
		return (ec);
		break;
		}
		else {*/
		ec = 0;
		return (ec);
		//break;
		//}
	//	}
		break;

		case 99: //clearmem
		ec = 0; 
		return (ec);
		break;
		
		case 117: //sin
		ec = 0; 
		return (ec);
		break;

		case 105: //cos
		ec = 0; 
		return (ec);
		break;

		case 111: //tg
		ec = 0; 
		return (ec);
		break;

		case 112: //ctg
		ec = 0; 
		return (ec);
		break;

		case 101: //expotentation
		ec = 0;
		return (ec);
		break;

		case 114: //square
		ec = 0;
		return (ec);
		break;
		
		case 118: //sqrt
		ec = 0;
		return (ec);
		break;

		case 104: //help
		ec = 0;
		return (ec);
		break;

		case 119:
		ec = 0;
		return (ec);
		break;

		case 113: //quit
		ec = 0;
		return (ec);
		break;
		}
	}

	return (ec);
}


/* Specific program functions (i.e. options) that do not take arguments
   Listed by precedency */
float sinus (void)
{
	if (mem == 1){
		printf("%c[1m%s****sin(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:%f\n", x);
		w = sin(x);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
mem = 0;
	}
	else{
		printf("%c[1m%s****sin(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:");
		scanf("%f", &x);
		w = sin(x);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
	}
	return(w);
}	

float cosinus (void)
{
	if (mem == 1){
		printf("%c[1m%s****cos(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:%f\n", x);
		w = cos(x);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
mem = 0;
	}
	else{
		printf("%c[1m%s****cos(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:");
		scanf("%f", &x);
		w = cos(x);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
	}
	return(w);
}	

float tang (void)
{
	if (mem == 1){
		printf("%c[1m%s****tg(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:%f\n", x);
		w = tan(x);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
mem = 0;
	}
	else{
		printf("%c[1m%s****tg(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:");
		scanf("%f", &x);
		w = tan(x);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
	}
	return(w);
}	

float kutang (void)
{
	if (mem == 1){
		printf("%c[1m%s****ctg(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:%f\n", x);
		w = 1/(tan(x));
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
mem = 0;
	}
	else{
		printf("%c[1m%s****ctg(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:");
		scanf("%f", &x);
		w = 1/(tan(x));
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
	}
	return(w);
}	

float exponent (void)
{
	int i;

	if (mem == 1){
		printf("%c[1m%s****Exponentation(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:%f\n", x);
		printf("Exponent:");
		scanf("%f", &y);
		fp = fopen("expdata.txt", "w");
		for(i=1; i<=y; ++i) {
		w = pow(x, i);
		fprintf(fp, "%f ", w);
		printf("%i. %f\n", i, w);
		}
		fclose(fp);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
mem = 0;
	}
	else{
		printf("%c[1m%s****Exponentation****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:");
		scanf("%f", &x);
		printf("Exponent:");
		scanf("%f", &y);
		fp = fopen("expdata.txt", "w");
		//fprintf(fp, "");
		for(i=1; i<=y; ++i) {
		w = pow(x, i);
		fprintf(fp, "%f ", w);
		printf("%i. %f\n", i, w);
		}
		fclose(fp);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
	}
	return(w);
}

float sroot (void)
{
	if (mem == 1){
		printf("%c[1m%s****Square root(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:%f\n", x);
		w = sqrt (x);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
mem = 0;
	}
	else{
		printf("%c[1m%s****Square root****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:");
		scanf("%f", &x);
		w = sqrt (x);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
	}
	return(w);
}	

float square (void)
{
	if (mem == 1){
		printf("%c[1m%s****Square(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:%f\n", x);
		w = sq(x);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
mem = 0;
	}
	else{
		printf("%c[1m%s****Square****%c[0m\n", ESC, KGRN, ESC);
		printf("Base:");
		scanf("%f", &x);
		w = sq(x);
		printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
	}
	return(w);
}

float addition (void)
{
	if (mem == 1){
		printf("%c[1m%s****Addition M+****%c[0m\n", ESC, KGRN, ESC);
		printf("First number:%f\n", x);
		printf("Second number:");
		scanf("%f", &y);
		w = sum(x, y);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
mem = 0;
	}
	else{
		printf("%c[1m%s****Addition****%c[0m\n", ESC, KGRN, ESC);
		printf("First number:");
		scanf("%f", &x);
		printf("Second number:");
		scanf("%f", &y);
		w = sum(x, y);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
	}
	return(w);
}

float substraction (void)
{
	if (mem == 1){
		printf("%c[1m%s****Substraction M+****%c[0m\n", ESC, KGRN, ESC);
		printf("First number:%f\n", x);
		printf("Second number:");
		scanf("%f", &y);
		w = dif(x, y);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
mem = 0;
	}
	else{
		printf("%c[1m%s****Substraction****%c[0m\n", ESC, KGRN, ESC);
		printf("First number:");
		scanf("%f", &x);
		printf("Second number:");
		scanf("%f", &y);
		w = dif(x, y);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
	}
	return(w);
}

float multiply (void)
{
	if (mem == 1){
		printf("%c[1m%s****Multiplication(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("First number:%f\n", x);
		printf("Second number:");
		scanf("%f", &y);
		w = prod(x, y);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
mem = 0;
	}
	else{
		printf("%c[1m%s****Multiplication****%c[0m\n", ESC, KGRN, ESC);
		printf("First number:");
		scanf("%f", &x);
		printf("Second number:");
		scanf("%f", &y);
		w = prod(x, y);
		printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
	}
	return(w);
}

float division (void)
{
	if (mem == 1){
		printf("%c[1m%s****Division(x)****%c[0m\n", ESC, KGRN, ESC);
		printf("First number:%f\n", x);
		printf("Second number:");
		scanf("%f", &y);
		w = quot(x, y);
	printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
		option = 0;
mem = 0;
		}
	else{
		printf("%c[1m%s****Division****%c[0m\n", ESC, KGRN, ESC);
		printf("First number:");
		scanf("%f", &x);
		printf("Second number:");
		scanf("%f", &y);
		w = quot(x,y);
				printf("\n%c[1mResult:%s%f\n%c[0mResult is stored in memory.\n\n", ESC, KYEL, w, ESC);
	
	}
	return(w);
}

/*int logit(void)
{
	if (islog == 0){
	islog = 1;
	while (islog == 1){
	freopen ("log.txt", "w", stdout);
	break;		
		}
	}
	else{
		islog = 0;
	while (islog == 0){

		fclose(stdout);
	}
}
	return islog;
}*/

/* Options switch (if errorcheck was passed then this function is called to execute proper option based on input)*/

char options (void)
{
	char help[1024];
	int ch = 0;
	//int i;
	switch (option) {

		case 99: //cls
		w = 0; 
		mem = 0;
		break;

		case 63: //mem query
		printf("%sLast stored number:%f\n%s", KGRN, wcp, KWHT);	
		break;

		case 120:
		//if (mem == 0){
		x = w;
		mem = 1;
		printf("Result used as first number.\n");
		wcp = w;
		w = 0;
		/*}
		else {
		mem = 0;
		perror("nope.\n");
		}*/
		break;

		case 117: //sin
		sinus();
		break;

		case 105: //cos
		cosinus();
		break;

		case 111: //tg
		tang();
		break;

		case 112: //ctg
		kutang();
		break;

		case 101:
		exponent();
		break;

		case 118:
		sroot();
		break;

		case 114:
		square();
		break;

		case 97:
		addition();
		break;

		case 115:
		substraction();
		break; 

		case 109:
		multiply();
		break;

		case 119:
		printf("%s", CLS);
		break;

		case 100:
		division();
		break;
		
		/*case 108:
		logit();
		if (islog == 1){
			printf("Logging.\n");
			}
		else{
			printf("Stopped logging.\n");
		}
		break;*/

		case 104: //Reading help file (if something changes, it will be displayed)
		if ((fp=fopen("help.txt", "r"))==NULL) {
     		printf ("Can't open help file!\n");
    		break;
     		}

		fp=fopen("help.txt", "r");
	
		do{
  			fgets(help, 1024, fp);
  			ch = feof(fp);
  			if(ch == 0){
			fputs(help, stdout);
 
  			}
  			else break; 
  
  			}while(!feof(fp));
		fclose(fp);
		break;

		case 113:
		
		printf("%c[40m%s", ESC, CLS);
		printf("QUIT%c[40m\n", ESC);
		exit(0);
		return 1;
		break;
		}
	return 0;
}		

int main (void)
{
	printf("%s", CLS);
	printf("%c[45m", ESC);
	//freopen("error.txt", "w", stderr);
	//int check = 0;
	printf("%c[1m********************************************%c[45m\n", ESC, ESC);
	printf("%snoob%sCALC %sby Pawel Mazurkiewicz (c)2013\n", KGRN, KRED, KWHT);
	printf("%c[1m%sVersion 0.3\n%c[0m", ESC, KBLU, ESC);
	printf("%c[45mCheck HELP for options\n", ESC);
	printf("h for Help\n");
	printf("q for Quit\n");
	printf("********************************************\n%c[0m", ESC);
	/*Main program's loop - ends when user input q(113) in option prompt */
	do{
	//fprintf(stderr, "Invalid option.\n");
	printf("%c[45mOption or expression:\n\a", ESC);
	//option = getchar();
	scanf("%c", &option);
	//check = errorcheck(0);
	//if (check == 0){
	if (errorcheck(0) > 0) printf("Invalid option\n");
	if (!errorcheck(0)){
	options();
	option = 0;
	}
	//option = getchar();
	scanf("%c", &option);
	}while (option !=113);		

	

return 0;
}
